import React from "react";

export default class NavigationFooter extends React.Component {

    render() {
        return <ul id="navigation-footer">
            <li><a href="javascript:window.history.back()" title="Back"><i
                className="fa fa-caret-square-o-left"></i>Back</a></li>
            <li><a href="javascript:window.scrollTo(0, 0)" title="Scroll to top"><i
                className="fa fa-caret-square-o-up"></i>Scroll to top</a></li>
        </ul>;
    }

}