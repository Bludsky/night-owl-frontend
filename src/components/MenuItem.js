import React from "react";
import {Link, RouteHandler} from "react-router";

export default class MenuItem extends React.Component {

    render() {
        return (
            <li>
                <Link to={this.props.title} className={(this.props.active) ? "active": ""}><span>{this.props.title}</span></Link>
                <RouteHandler />
            </li>
        );
    }

}