import React from "react";
import Select from "react-select";
import Request from "superagent";
import InlineCss from "react-inline-css";
import Router from "react-router";
import {REST_URL} from "config/Config";

export default class SearchBox extends React.Component {

    onChange(str, selected) {
        if (selected.length > 0) {
            this.context.router.transitionTo(`/${selected[0].value}`);
        }
    }

    getOptions(input, callback) {
        var opts = {
            options: [],
            complete: false
        }

        if (input == null || input.length < 2) {
            callback(null, opts);
            return;
        }

        // Artificial delay to prevent stress calls
        setTimeout(() => {
            Request.get(`${REST_URL}/articles/searchByTitle`)
            .query({title: input})
            .end((err, res) => {
                    if (err != null) {
                        // Infinite spinning upon error
                        return;
                    }
                    var json = JSON.parse(res.text);
                    json.content.map(o => {
                        opts.options.push({
                            label: o.title,
                            value: o.sanitizedTitle
                        })
                    });
                    callback(null, opts);
                });
        }, 500);
    }

    static css() {
        return (`
        & {
        float: right;
        }
        &>div {
        width: 300px;
        left: 5px;
        bottom: 9px;
        }
        `);
    }

    render() {
        return <InlineCss stylesheet={SearchBox.css()} namespace="SearchBox"><Select
            onChange={this.onChange.bind(this)} asyncOptions={this.getOptions} cacheAsyncResults={false} autoload={false} placeholder="Search Night Owl..." /></InlineCss>;
    }

}

SearchBox.contextTypes = {
    router: React.PropTypes.func.isRequired
};