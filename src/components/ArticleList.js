import React from "react";
import Waypoint from "react-waypoint";
import Request from "superagent";
import {REST_URL} from "config/Config";

// Components
import ArticleItem from "components/ArticleItem";
import Loader from "components/Loader";

export default class ArticleList extends React.Component {

    constructor() {
        super();
        this.state = {
            items: [],
            page: 0,
            isLoading: false,
            isFinished: false
        }
    }

    componentWillReceiveProps(nextProps) {
        this.state = {
            items: [],
            page: 0
        }
        this.loadBatch(nextProps);
    }

    loadBatch(nextProps) {
        // Determine whether called via initial load or upon props receive
        var selectedTags = (typeof nextProps === 'undefined') ? this.props.selectedTags : nextProps.selectedTags;
        var orderBy = (typeof nextProps === 'undefined') ? this.props.orderBy : nextProps.orderBy;
        var url = `${REST_URL}/articles/search/`;
        var tags = [];
        if (typeof selectedTags != "undefined" && selectedTags.length > 0) {
            selectedTags.map(o => {
                tags.push(o.value);
            });
            url = url.concat("findByVisibleIsTrueAndTagsNameIn");
        } else {
            url = url.concat("findByVisibleIsTrue");
        }
        this.setState({isLoading: true});
        Request.get(url)
            .query({
                size: 10,
                page: this.state.page,
                sort: orderBy,
                tags: tags.join(),
                projection: "featureList"
            })
            .end((err, res) => {
                var json = JSON.parse(res.text);
                if (typeof json._embedded != "undefined") {
                    this.state.items = this.state.items.concat(json._embedded.articles);
                    this.state.page++;
                } else {
                    this.state.isFinished = true;
                }
                this.setState({isLoading: false});
            });
        return [];
    }

    renderWaypoint() {
        if (this.state.isLoading) {
            return <Loader />;
        } else if (this.state.isFinished) {
            return;
        }

        return (
            <Waypoint
                onEnter={this.loadBatch.bind(this)}
                threshold={0.05}
                />
        )
    }

    render() {
        return (
            <div style={{position: "relative"}}>
                {this.state.items.map((item, i) => {
                    return <ArticleItem key={"Article" + i} data={item}/>
                })}
                {this.renderWaypoint()}
            </div>
        );
    }

}