'use strict';
var React = require('react');

module.exports = React.createClass({
	displayName: 'EditableDiv',

	propTypes: {
		content: React.PropTypes.string.isRequired,
		onChange: React.PropTypes.func.isRequired
	},

	getInitialState: function() {
		// this is anti-pattern but we treat this.props.content as initial content
		return {html: this.props.content};
	},

	emitChange: function() {
		var editor = this.refs.editor.getDOMNode(),
			newHtml = editor.innerHTML;

		this.setState({html: newHtml}, function() {
			this.props.onChange({
				target: {
					value: newHtml
				}
			});
		}.bind(this));
	},

	componentWillReceiveProps: function(nextProps) {
		this.setState({
			html: nextProps.content
		});
	},

	shouldComponentUpdate: function(nextProps) {
        return nextProps.content !== this.state.html;
    },

    execCommand: function(command, arg) {
    	document.execCommand(command, false, arg);
    },

	render: function() {
		// customize css rules here
		var buttonSpacing = {marginRight: 2},
			toolbarStyle = {marginBottom: 3};

		return (
			<div>
				<div style={toolbarStyle}>
					<div className="btn-group btn-group-xs" role="group" style={buttonSpacing}>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'formatBlock', 'P')}>
                            <i className="fa fa-paragraph"></i>
                        </button>
						<button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'formatBlock', 'BLOCKQUOTE')}>
                            <i className="fa fa-quote-left"></i>
                        </button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'formatBlock', 'H1')}>
                            <i className="fa fa-header">1</i>
                        </button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'formatBlock', 'H2')}>
                            <i className="fa fa-header">2</i>
                        </button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'formatBlock', 'H3')}>
                            <i className="fa fa-header">3</i>
                        </button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'formatBlock', 'H4')}>
                            <i className="fa fa-header">4</i>
                        </button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'formatBlock', 'H5')}>
                            <i className="fa fa-header">5</i>
                        </button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'formatBlock', 'H6')}>
                            <i className="fa fa-header">6</i>
                        </button>
						<button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'italic')}>
							<i className="fa fa-code"></i>
						</button>
						<button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'bold')}>
							<i className="fa fa-bold"></i>
						</button>
						<button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'underline')}>
							<i className="fa fa-underline"></i>
						</button>
						<button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'strikeThrough')}>
							<i className="fa fa-strikethrough"></i>
						</button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'fontSize', 1)}>
							<i className="fa fa-font">1</i>
						</button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'fontSize', 2)}>
                            <i className="fa fa-font">2</i>
                        </button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'fontSize', 3)}>
                            <i className="fa fa-font">3</i>
                        </button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'fontSize', 4)}>
                            <i className="fa fa-font">4</i>
                        </button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'fontSize', 5)}>
                            <i className="fa fa-font">5</i>
                        </button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'fontSize', 6)}>
                            <i className="fa fa-font">6</i>
                        </button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'fontSize', 7)}>
                            <i className="fa fa-font">7</i>
                        </button>
					</div>

					<div className="btn-group btn-group-xs" role="group" style={buttonSpacing}>
						<button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'insertOrderedList')}>
							<i className="fa fa-list-ol"></i>
						</button>
						<button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'insertUnorderedList')}>
							<i className="fa fa-list-ul"></i>
						</button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'justifyLeft')}>
                            <i className="fa fa-align-left">left</i>
                        </button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'justifyRight')}>
                            <i className="fa fa-align-left">right</i>
                        </button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'justifyCenter')}>
                            <i className="fa fa-align-left">center</i>
                        </button>
                        <button type="button" className="btn btn-default" onClick={this.execCommand.bind(this, 'justifyFull')}>
                            <i className="fa fa-align-left">justify</i>
                        </button>
                        <button
                            type="button"
                            className="btn btn-default btn-xs"
                            onClick={this.execCommand.bind(this, 'removeFormat')}>
                            <i className="fa fa-eraser"></i>
                        </button>
					</div>
				</div>

				<div
					ref="editor"
					className="form-control"
					{...this.props}
					contentEditable="true"
					dangerouslySetInnerHTML={{__html: this.state.html}}
					onInput={this.emitChange} />
			</div>
		);
	}
});