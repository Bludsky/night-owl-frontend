import React from "react";
import InlineCss from "react-inline-css";
import {REST_URL} from 'config/Config.js';

export default class SideMenu extends React.Component {

    render() {
        return <InlineCss stylesheet={SideMenu.css()} namespace="SocialBox">
            <ul>
                <li><a href={REST_URL + "/rss"} className="rss"/></li>
                <li><a href="https://twitter.com/Nightowl_it" className="twitter"/></li>
                <li><a href="https://www.facebook.com/Nightowlit-880655818718665" className="facebook"/></li>
                <li><a href="https://plus.google.com/112420169282109205902" className="google"/></li>
                <li><a href="https://www.linkedin.com/groups/8452555" className="linkedin"/></li>
            </ul>
        </InlineCss>;
    }

    static css() {
        return (`
        @-webkit-keyframes hb {
            0% { -webkit-transform: scale(1); }
            50% { -webkit-transform: scale(0.9); }
            100% { -webkit-transform: scale(1); }
        }
        & ul {
            list-style-type: none;
            padding: 0;
            margin: 35px 0;
        }
        & ul li {
            display: inline;
            margin: 5px;
        }
        & ul li a {
            width: 32px;
            height: 32px;
            display: inline-block;
        }
        & ul li a:hover {
            -webkit-animation-name: hb;
            -webkit-animation-duration: 200ms;
            -webkit-transform-origin:50% 50%;
            -webkit-animation-iteration-count: 2;
            -webkit-animation-timing-function: linear;
        }
        & ul li a.rss {
            background: url('/img/icons/rss.png') no-repeat;
        }
        & ul li a.twitter {
            background: url('/img/icons/twitter.png') no-repeat;
        }
        & ul li a.facebook {
            background: url('/img/icons/facebook.png') no-repeat;
        }
        & ul li a.google {
            background: url('/img/icons/google.png') no-repeat;
        }
        & ul li a.linkedin {
            background: url('/img/icons/linkedin.png') no-repeat;
        }
        `);
    }

}