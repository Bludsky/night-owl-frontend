import React from "react";
import Request from "superagent";
import Select from "react-select";

import {REST_URL} from "config/Config";

// Components
import SelectField from 'components/SelectField';
import SocialBox from 'components/SocialBox';

const ORDER_OPTIONS = [
    {label: 'By date ▲', value: 'created,asc'},
    {label: 'By date ▼', value: 'created,desc'},
    {label: 'By title ▲', value: 'title,asc'},
    {label: 'By title ▼', value: 'title,desc'}
];

export default class SideMenu extends React.Component {

    constructor() {
        super();
        this.state = {
            tags: [],
            tagHint: "Multiple tags supported"
        };
    }

    handleTagChange(selectedTags) {
        this.props.tagHandler(selectedTags);
    }

    handleOrderByChange(orderBy) {
        this.props.orderByHandler(orderBy[0]);
    }

    componentDidMount() {
        Request.get(`${REST_URL}/tags/aggregated`)
            .end((err, res) => {
                if (err != null) {
                    this.setState({
                        tagHint: "Failed to load tag data"
                    });
                    return;
                }
                var json = JSON.parse(res.text);
                if (typeof json.content != "undefined") {
                    json.content.map(tag => {
                        this.state.tags.push({
                            label: `${tag.name} (${tag.occurrence})`,
                            value: tag.name
                        })
                    });
                }
                this.forceUpdate();
            });
    }

    render() {
        return (
            <div>
                <SelectField multi={true} placeholder="Select tags..." label="Filter by tags"
                             handler={this.handleTagChange.bind(this)}
                             options={this.state.tags} selected={this.props.selectedTags} hint={this.state.tagHint}/>
                <SelectField multi={false} placeholder="Order entries..." label="Order entries"
                             handler={this.handleOrderByChange.bind(this)}
                             options={ORDER_OPTIONS} selected={this.props.activeOrderBy}/>
                <SocialBox />
            </div>
        );
    }

}