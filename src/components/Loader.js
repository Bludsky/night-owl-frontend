import React from "react";
import InlineCss from "react-inline-css";

export default class Loader extends React.Component {

    render() {
        return (
            <InlineCss stylesheet={Loader.css()} namespace="Loader" />
        );
    }

    static css() {
        return (`
            & {
            width: 128px;
            height: 15px;
            display: block;
            margin-left: 30%;
            background-image: url("/img/loader.gif");
            }
        `);
    }

}