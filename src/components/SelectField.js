import React from 'react';
import Select from 'react-select';

export default class SelectField extends React.Component {
    onLabelClick(data, event) {
        // TODO tag info (popup?)
    }

    onChange(str, selected) {
        this.props.handler(selected);
    }

    renderHint() {
        if (!this.props.hint) {
            return null;
        }
        return (
            <div className="hint">{this.props.hint}</div>
        );
    }

    render() {
        return (
            <div className="section">
                <h3 className="section-heading">{this.props.label}</h3>
                <Select
                    allowCreate={this.props.allowCreate}
                    onOptionLabelClick={this.onLabelClick}
                    value={this.props.selected}
                    multi={this.props.multi}
                    placeholder={this.props.placeholder}
                    options={this.props.options}
                    onChange={this.onChange.bind(this)}/>
                {this.renderHint()}
            </div>
        );
    }
};