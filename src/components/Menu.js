import React from "react";
import InlineCss from "react-inline-css";
import * as MenuItems from "constants/menuItems";

// Components
import MenuItem from "components/MenuItem";
import SearchBox from "components/SearchBox";

const menuItems = [
    MenuItems.SECTION_BLOG,
    MenuItems.SECTION_LUCKY,
    MenuItems.SECTION_ABOUT
];

export default class Menu extends React.Component {

    render() {
        return (
            <InlineCss stylesheet={Menu.css()} namespace="Menu">
                <div id="wrapper">
                    <div id="night-owl"/>
                    <ul>
                        {
                            menuItems.map((o, i) => <MenuItem key={"MenuItem" + i} title={o}
                                                              active={this.props.active == o}/>)
                        }
                    </ul>
                    <ul id="right">
                        <li><span className="highlight">Q</span>uick <span className="highlight">A</span>ccess:
                            <SearchBox /></li>
                    </ul>
                </div>
            </InlineCss>
        );
    }

    static css() {
        return (`
            & {
                width: 100%;
                height: 50px;
                position: fixed;
                top: 0px;
                z-index: 100;
                color: #fafafa;
                font-family: 'Lato', 'Helvetica Neue', Helvetica, sans-serif;
                background-color: #222;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.5);
            }
            & div#wrapper {
                width: 80%;
                margin-left: auto;
                margin-right: auto;
            }
            & ul#right {
                position: fixed;
                left: 60%;
                margin-left: 0px;
                padding-left: 0px;
                float: right;
            }
            & ul#right li>span.highlight {
                font-weight: bold;
                color: #6467B5;
            }
            & ul {
                display: inline-block;
            }
            & ul li {
                display: inline;
            }
            & ul li a {
                padding: 15px 15px 13px 15px;
                font-size: 1.1em;
                color: #fafafa;
                text-decoration: none;
            }
            & ul li a:hover, ul li a.active{
                background-color: #333;
                border-bottom: 3px solid #8897CC;
            }
            & ul#right li a:hover {
                border-bottom: 3px solid lightpink;
            }
        `);
    }

}