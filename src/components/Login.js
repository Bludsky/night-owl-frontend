import React from "react";
import InlineCss from "react-inline-css";
import Request from "superagent";
import Cookie from 'react-cookie';
import {REST_URL} from "config/Config";
import LoginUtils from "utils/LoginUtils";

export default class Login extends React.Component {

    constructor() {
        super();
        this.state = {
            form_username: '',
            form_password: '',
            status: ''
        }
    }

    handleLogin() {
        var username = this.state.form_username;
        var password = this.state.form_password;
        if (username.trim().length < 1) {
            this.setState({status: 'Username cannot be blank'})
            return;
        }
        if (password.trim().length < 1) {
            this.setState({status: 'Password cannot be blank'})
            return;
        }

        var credentials = {
            username: username,
            password: password
        };

        Request.post(`${REST_URL}/auth/login`)
            .send(JSON.stringify(credentials))
            .end((err, res) => {
                if (err != null) {
                    console.log(res.status);
                    this.setState({status: `${res.status}: could not authorize`});
                } else {
                    LoginUtils.saveToken(res.text);
                    this.props.callback();
                }
            });
    }

    onUsernameChange(e) {
        this.setState({form_username: e.target.value});
    }

    onPasswordChange(e) {
        this.setState({form_password: e.target.value});
    }

    render() {
        return (
            <InlineCss stylesheet={Login.css()} namespace="Login">
                <div className="row">
                    <div className="cell">
                        <label htmlFor="username">Username</label>
                    </div>
                    <div className="cell">
                        <input type="text" id="username" onChange={this.onUsernameChange.bind(this)} value={this.state.form_username}/>
                    </div>
                </div>
                <div className="row">
                    <div className="cell">
                        <label htmlFor="password">Password</label>
                    </div>
                    <div className="cell">
                        <input type="password" id="password" onChange={this.onPasswordChange.bind(this)} value={this.state.form_password}/>
                    </div>
                </div>
                <div className="row">
                    <div className="cell">
                        <a href="#" className="button" onClick={this.handleLogin.bind(this)}>Login</a>
                    </div>
                    <div className="cell info">
                        {this.state.status}
                    </div>
                </div>
            </InlineCss>
        );
    }


    static css() {
        return (`
        & {
            width: 40%;
            display: table;
            border: 1px dashed #d4d4d4;
            padding: 15px;
        }
        label {
            cursor: pointer;
        }
        div.row {
            display: table-row;
            margin-bottom: 10px;
        }
        div.cell {
            width: 50%;
            display: table-cell;
        }
        div.row a.button {
            margin: 0;
            position: relative;
            top: 5px;
        }
        div.cell.info {
            font-size: 0.7em;
        }
        `);
    }

}