import React from "react";
import {Link, RouteHandler} from "react-router";

export default class TagBar extends React.Component {

    render() {

        var tagLine;
        if (this.props.data.tags == null) {
            tagLine = "No tags";
        } else {
            tagLine = this.props.data.tags.map((tag, i) => {
                return tag.name + (this.props.data.tags.length - 1 > i ? ", " : "");
            });
        }

        return <ul className="meta">
            <li className="date">{this.props.data.created.dayOfMonth.pad()}/{this.props.data.created.monthValue.pad()}/{this.props.data.created.year}</li>
            <li className="tag">
                {tagLine}
            </li>
            {(this.props.displayAuthor) ? <li className="author"><Link
                to={`/author/${this.props.data.sanitizedAuthor}`}>{this.props.data.author}</Link><RouteHandler /></li> : ""}
            {(this.props.displayPatchLink) ? <li><Link
                to={`/admin?title=${this.props.data.sanitizedTitle}`}>Patch link</Link><RouteHandler /></li> : ""}
        </ul>;
    }
}

Number.prototype.pad = function (size) {
    var str = String(this);
    while (str.length < (size || 2)) {
        str = "0" + str;
    }
    return str;
};