import React from "react";
import InlineCss from "react-inline-css";
import {Link, RouteHandler} from "react-router";

// Components
import TagBar from "components/TagBar";

export default class ArticleItem extends React.Component {

    render() {
        return (
            <InlineCss stylesheet={ArticleItem.css()}>
                <h2><Link to={`/${this.props.data.sanitizedTitle}`}>{this.props.data.title}</Link></h2>
                <TagBar data={this.props.data} displayAuthor={true} />
                <p>{this.props.data.description}</p>
                <RouteHandler />
            </InlineCss>
        );
    }

    static css() {
        return (`
            & {
                width: 80%;
                margin-bottom: 30px;
            }
            & h2 a {
                color: #484848;
                text-decoration: none;
                border-bottom: 2px solid #C6C1C1;
            }
            & h2 a:hover{
                color: #484848;
                border-color: #484848;
            }
            & p {
                font-size: 1.01em;
            }
        `)
    }

}