import {Server} from "hapi";
import React from "react";
import Router from "react-router";
import Transmit from "react-transmit";
import routes from "views/Routes";
import url from "url";
import {REST_URL} from 'config/Config.js';

var hostname = process.env.HOSTNAME || "localhost";

/**
 * Start Hapi server on port 80.
 */
const server = new Server();
server.connection({host: hostname, port: process.env.PORT || 80});
server.start(function () {
    console.info("==> ✅  Server is listening");
    console.info("==> 🌎  Go to " + server.info.uri.toLowerCase());
});

/**
 * Hapi router serve static files.
 */
server.route({
    method: "*",
    path: "/{params*}",
    handler: (request, reply) => {
        reply.file("static" + request.path);
    }
});

/**
 * Hapi router setup non-www to www redirect.
 */
server.route({
    method: "*",
    path: "/{params*}",
    vhost: hostname,
    handler: (request, reply) => {
        reply.redirect('http://www.' + hostname + request.url.path);
    }
});

/**
 * Hapi router forward to sitemap.
 */
server.route({
    method: "*",
    path: "/sitemap",
    handler: (request, reply) => {
        reply.redirect(REST_URL + "/sitemap");
    }
});

/**
 * Hapi router forward to rss.
 */
server.route({
    method: "*",
    path: "/rss",
    handler: (request, reply) => {
        reply.redirect(REST_URL + "/rss");
    }
});

/**
 * Catch dynamic requests here to fire-up React Router.
 */
server.ext("onPreResponse", (request, reply) => {
    if (typeof request.response.statusCode !== "undefined") {
        return reply.continue();
    }

    Router.run(routes, request.path, (Handler, router) => {
        Transmit.renderToString(Handler).then(({reactString, reactData}) => {
            let output = (
                `<!doctype html>
				<html lang="en-us">
					<head>
					    <meta name="description" content="Java software development blog.">
					    <meta name="keywords" content="java,spring,hibernate,software,developer">
					    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
					    <meta http-equiv="Content-language" content="en">
						<title>Night Owl</title>
						<link rel="shortcut icon" href="/favicon.ico" />
						<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css' />
						<link href='/css/default.css' rel='stylesheet' type='text/css' />
						<link href='/css/react-select.css' rel='stylesheet' type='text/css' />
						<link href="/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type='text/css' />
					</head>
					<body">
						<div id="container">${reactString}</div>
					</body>
				</html>`
            );

            const webserver = process.env.NODE_ENV === "production" ? "" : "//" + hostname + ":80";
            output = Transmit.injectIntoMarkup(output, reactData, [`${webserver}/dist/client.js`]);

            reply(output);
        }).catch((error) => {
            reply(error.stack).type("text/plain").code(500);
        });
    })
});
