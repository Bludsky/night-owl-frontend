import React from "react";
import Transmit from "react-transmit";
import Router from "react-router";
import Request from "superagent";
import InlineCss from "react-inline-css";
import {REST_URL, UA} from "config/Config";
import {Link, RouteHandler} from "react-router";
import GoogleAnalytics from "react-g-analytics";

// Components
import Menu from "components/Menu";
import Loader from "components/Loader";
import NavigationFooter from "components/NavigationFooter";

class Author extends React.Component {

    constructor() {
        super();
        this.state = {
            isLoaded: false,
            isNotFound: false,
            data: []
        }
    }

    componentDidUpdate() {
        if (!this.state.isNotFound) {
            document.title = this.state.data.articles[0].author;
        } else {
            document.title = "Entry not found :(";
        }
    }

    componentDidMount() {
        document.title = "Loading Author";

        var author = this.context.router.getCurrentParams().author;
        Request.get(`${REST_URL}/articles/search/findBySanitizedAuthorAndVisibleIsTrue`)
            .query({
                sanitizedAuthor: author,
                projection: "simpleList"
            })
            .end((err, res) => {
                var json = JSON.parse(res.text);
                if (typeof json._embedded != "undefined") {
                    this.setState({
                        isLoaded: true,
                        data: json._embedded
                    });
                } else {
                    this.setState({
                        isLoaded: true,
                        isNotFound: true
                    });
                }
            });
    }

    renderList() {
        if (!this.state.isLoaded) {
            return <Loader />;
        }
        if (this.state.isNotFound) {
            return "Author not found :(";
        }
        var data = this.state.data;
        return (
            <InlineCss stylesheet={Author.css()} namespace="Author">
                <h2>{data.articles[0].author}</h2>
                <ul>
                    {data.articles.map((o, i) => {
                        return <li><Link key={"Author" + i} to={`/${o.sanitizedTitle}`}
                                         className="link">{o.title}</Link></li>;
                    })}
                </ul>
                <RouteHandler />
            </InlineCss>
        );
    }

    render() {
        return (
            <div>
                <Menu />

                <div id="content">
                    <div id="inner">
                        {this.renderList()}
                        <NavigationFooter />
                    </div>
                </div>
                <GoogleAnalytics id={UA}/>
            </div>
        );
    }

    static css() {
        return (`
            &>ul {
                list-style-type: none;
                padding-left: 25px;
            }
            &>ul li {
                margin-bottom: 7px;
            }
        `);
    }

}

Author.contextTypes = {
    router: React.PropTypes.func.isRequired
};

export default Transmit.createContainer(Author);