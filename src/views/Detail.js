import React from "react";
import Transmit from "react-transmit";
import Router from "react-router";
import Request from "superagent";
import LoginUtils from "utils/LoginUtils";
import {REST_URL, UA} from "config/Config";
import * as MenuItems from "constants/menuItems";
import GoogleAnalytics from "react-g-analytics";

// Components
import Menu from "components/Menu";
import TagBar from "components/TagBar";
import Loader from "components/Loader";
import NavigationFooter from "components/NavigationFooter";

class Detail extends React.Component {

    constructor() {
        super();
        this.state = {
            data: [],
            isLoaded: false,
            isNotFound: false
        }
    }

    // For transition from quick access
    componentWillReceiveProps() {
        this.load();
    }

    componentDidMount() {
        document.title = "Loading Article";
        this.load();
    }

    componentDidUpdate() {
        if (!this.state.isNotFound) {
            document.title = this.state.data.title;
        } else {
            document.title = "Entry not found :(";
        }
    }

    load() {
        var title = this.context.router.getCurrentParams().title;
        Request.get(`${REST_URL}/articles/search/findBySanitizedTitleAndVisibleIsTrue`)
            .query({sanitizedTitle: title})
            .end((err, res) => {
                if (res.text != "") {
                    this.state.data = JSON.parse(res.text);
                    this.setState({
                        isLoaded: true
                    });
                } else {
                    this.setState({
                        isLoaded: true,
                        isNotFound: true
                    });
                }
            });
    }

    resolveActive() {
        if (typeof this.context.router.getCurrentQuery().lucky != 'undefined') {
            return MenuItems.SECTION_LUCKY;
        } else {
            return MenuItems.SECTION_BLOG;
        }
    }

    renderEntry() {
        if (!this.state.isLoaded) {
            return <Loader />;
        }
        if (this.state.isNotFound) {
            return "Entry not found :(";
        }
        var data = this.state.data;
        // Sanitized html to be injected
        var markup = {__html: data.content};

        return (
            <div>
                <h1>{data.title}</h1>
                <TagBar data={data} displayAuthor={true} displayPatchLink={LoginUtils.isAuthorized()}/>
                <blockquote>{data.description}</blockquote>
                <div className="article" dangerouslySetInnerHTML={markup}/>
            </div>
        );
    }

    render() {
        return (
            <div>
                <Menu active={this.resolveActive()}/>

                <div id="content">
                    <div id="inner">
                        {this.renderEntry()}
                        <NavigationFooter />
                    </div>
                </div>
                <GoogleAnalytics id={UA}/>
            </div>
        );
    }

}

Detail.contextTypes = {
    router: React.PropTypes.func.isRequired
};

export default Transmit.createContainer(Detail);