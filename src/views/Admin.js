import React from "react";
import Transmit from "react-transmit";
import Router from "react-router";
import Request from "superagent";
import InlineCss from "react-inline-css";
import {REST_URL, UA} from "config/Config";
import LoginUtils from "utils/LoginUtils";
import GoogleAnalytics from "react-g-analytics";

// Components
import Menu from "components/Menu";
import Loader from "components/Loader";
import Login from "components/Login";
import EditableDiv from "components/EditableDiv";

const Mode = {INSERT: "Insert mode", PATCH: "Patch mode"};

class Admin extends React.Component {

    constructor() {
        super();
        this.state = {
            mode: Mode.INSERT,
            data: [],
            content: '',
            isLoaded: false,
            status: null,
            isAuthorized: LoginUtils.getToken() != null,
            form_title: '',
            form_author: '',
            form_description: '',
            form_tags: '',
            form_visible: false
        }
    }

    componentDidMount() {
        document.title = "Night Owl Admin";

        var title = this.context.router.getCurrentQuery().title;
        if (title == null || !this.state.isAuthorized) {
            this.setState({isLoaded: true});
            return;
        }

        Request.get(`${REST_URL}/articles/search/findBySanitizedTitle`)
            .query({sanitizedTitle: title})
            .set('X-AUTH-TOKEN', LoginUtils.getToken())
            .end((err, res) => {
                if (res.text != "") {
                    var json = JSON.parse(res.text);
                    this.setState({
                        mode: Mode.PATCH,
                        data: json,
                        content: json.content,
                        isLoaded: true,
                        form_title: json.title,
                        form_author: json.author,
                        form_description: json.description,
                        form_tags: this.tagsToString(json.tags),
                        form_visible: json.visible
                    });
                } else {
                    this.setState({
                        mode: Mode.INSERT,
                        isLoaded: true,
                        status: `No entry for ${title} found. Switched to ${Mode.INSERT}.`
                    });
                }
            });
    }

    tagsToString(tags) {
        if (tags == null || tags.length < 1) {
            return '';
        }
        var tagArr = [];
        tags.map(o => tagArr.push(o.name));
        return tagArr.join(', ');
    }

    stringToTags(str) {
        if (str == null || str.length < 1) {
            return [];
        }
        var tagArr = [];
        str.split(",").map(o => tagArr.push(
            {name: o.trim(),
            color: "#000000"}
        ));

        return tagArr;
    }

    buildJsonFromForm() {
        var processedContent = this.state.content.replace(/"/g, "\"");

        return {
            title: this.state.form_title.trim(),
            description: this.state.form_description.trim(),
            author: this.state.form_author.trim(),
            visible: this.state.form_visible,
            tags: this.stringToTags(this.state.form_tags),
            content: processedContent
        }
    }

    isValid(json) {
        if (json.title.length < 1) {
            this.state.status = "Title must not be empty";
            return false;
        } else if (json.author.length < 1) {
            this.state.status = "Author must not be empty";
            return false;
        } else if (json.description.length < 1) {
            this.state.status = "Description must not be empty";
            return false;
        }
        return true;
    }

    handleSubmit() {
        var json = this.buildJsonFromForm();

        if (!this.isValid(json)) {
            this.forceUpdate();
            return;
        }
        if (this.state.mode == Mode.INSERT) {
            this.handlePost(json);
        } else {
            this.handlePatch(json);
        }
    }

    handlePost(json) {
        Request.post(`${REST_URL}/articles`)
            .send(JSON.stringify(json))
            .set('Content-Type', 'application/json')
            .set('X-AUTH-TOKEN', LoginUtils.getToken())
            .end((err, res) => {
                if (res.ok) {
                    var sanitizedTitle = json.title.replace(/\W+/g, '-').toLowerCase();
                    if (json.visible) {
                        // If visible redirect straight to the entry
                        this.context.router.transitionTo(`/${sanitizedTitle}`);
                    } else {
                        this.setState({status: "Entry successfully posted"});
                    }
                } else {
                    this.setState({
                        status: res.text
                    });
                }
            });
    }

    handlePatch(json) {
        var id = this.state.data.id;
        Request.patch(`${REST_URL}/articles/${id}`)
            .send(JSON.stringify(json))
            .set('Content-Type', 'application/json')
            .set('X-AUTH-TOKEN', LoginUtils.getToken())
            .end((err, res) => {
                if (res.ok) {
                    this.setState({status: `Entry successfully patched [id: ${id}]`});
                } else {
                    this.setState({
                        status: res.text
                    });
                }
            });
    }

    handleChange(e) {
        this.setState({content: e.target.value});
    }

    onTitleChange(e) {
        this.setState({form_title: e.target.value});
    }

    onAuthorChange(e) {
        this.setState({form_author: e.target.value});
    }

    onTagsChange(e) {
        this.setState({form_tags: e.target.value});
    }

    onDescriptionChange(e) {
        this.setState({form_description: e.target.value});
    }

    onVisibleChange(e) {
        this.setState({form_visible: e.target.checked});
    }

    handleSuccessfulLogin() {
        this.setState({isAuthorized: true});
    }

    handleLogout() {
        LoginUtils.clearToken();
        this.setState({isAuthorized: false})
    }

    renderContent() {
        if (!this.state.isLoaded) {
            return <Loader />;
        }
        if (!this.state.isAuthorized) {
            return <Login callback={this.handleSuccessfulLogin.bind(this)} />;
        }

        var editorStyle = {
            overflow: 'auto',
            width: 300,
            height: 100,
            maxHeight: 100
        }

        return (
            <InlineCss stylesheet={Admin.css()} namespace="Admin">
                <div className="panel">
                    <label htmlFor="title">Title:</label> <input className="text" type="text" id="title" onChange={this.onTitleChange.bind(this)} value={this.state.form_title}/>
                </div>
                <div className="panel">
                    <label htmlFor="author">Author:</label> <input className="text" type="text" id="author" onChange={this.onAuthorChange.bind(this)} value={this.state.form_author}/>
                </div>
                <div className="panel">
                    <label htmlFor="tags">Tags:</label> <input className="text" type="text" id="tags" onChange={this.onTagsChange.bind(this)} value={this.state.form_tags}/>
                </div>
                <div className="panel">
                    <label htmlFor="description">Description:</label> <textarea id="description" onChange={this.onDescriptionChange.bind(this)} value={this.state.form_description}/>
                </div>
                <div className="panel">
                    <label htmlFor="visible">Visible:</label> <input type="checkbox" id="visible" onChange={this.onVisibleChange.bind(this)} checked={this.state.form_visible}/>
                </div>
                <div className="panel">
                    You are now in <strong>{this.state.mode}</strong>
                </div>
                {(this.state.status != null) ? <div className="panel">
                    Status: {this.state.status}
                </div> : ""}
                <EditableDiv content={this.state.content} onChange={this.handleChange.bind(this)} />

                <div className="panel centered">
                    <a href="#" onClick={this.handleLogout.bind(this)} className="button">Logout</a> <a href="#" className="button"
                                                               onClick={this.handleSubmit.bind(this)}>Submit</a>
                </div>
            </InlineCss>
        );
    }


    render() {
        return (
            <div>
                <Menu />

                <div id="content">
                    <div id="inner">
                        {this.renderContent()}
                    </div>
                </div>
                <GoogleAnalytics id={UA} />
            </div>
        );
    }

    static css() {
        return (`
            &>div.panel {
                width: 100%;
                display: block;
                margin: 10px 0 10px 0;
            }
            &>div.panel label {
                display: block;
            }
            &>div.panel.centered {
                text-align: center;
            }
            &>div.panel input.text {
                font-size: 1em;
                width: 100%;
            }
            &>div.panel textarea {
                font-size: 1em;
                width: 100%;
                height: 50px;
            }
        `);
    }

}

Admin.contextTypes = {
    router: React.PropTypes.func.isRequired
};

export default Transmit.createContainer(Admin);