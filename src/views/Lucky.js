import React from "react";
import Transmit from "react-transmit";
import Router from "react-router";
import Request from "superagent";
import {UA, REST_URL} from "config/Config";
import * as MenuItems from "constants/menuItems";
import GoogleAnalytics from "react-g-analytics";

// Components
import Menu from "components/Menu";
import Loader from "components/Loader";

class Lucky extends React.Component {

    componentDidMount() {
        document.title = "Trying Your Luck";
    }

    componentWillMount() {
        var entries = [];
        Request.get(`${REST_URL}/articles/searchByTitle`)
            .query({title: ""})
            .end((err, res) => {
                if (err != null) {
                    return;
                }
                var json = JSON.parse(res.text);
                json.content.map(o => {
                    entries.push(o.sanitizedTitle);
                });
                if (entries.length < 1) {
                    return;
                }
                // Transition to a random entry
                this.context.router.transitionTo(`/${entries[Math.floor(Math.random() * entries.length)]}?lucky=true`);
            });
    }

    render() {
        return (
            <div>
                <Menu active={MenuItems.SECTION_LUCKY} />

                <div id="content">
                    <div id="inner">
                        <Loader />
                    </div>
                </div>
                <GoogleAnalytics id={UA} />
            </div>
        )
    }
}

export default Transmit.createContainer(Lucky);

Lucky.contextTypes = {
    router: React.PropTypes.func.isRequired
};