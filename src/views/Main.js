import React from "react";
import Transmit from "react-transmit";
import Cookie from "react-cookie";
import {UA} from "config/Config.js";
import GoogleAnalytics from "react-g-analytics";

// Components
import Menu from "components/Menu";
import ArticleList from "components/ArticleList";
import SideMenu from "components/SideMenu";

const DEFAULT_ORDER_BY = {label: 'By date ▼', value: 'created,desc'};
const COOKIE_FILTER_NAME = "filterState";

class Main extends React.Component {

    constructor() {
        super();
        this.state = this.loadFilterState();
    }

    componentDidMount() {
        document.title = "Night Owl";
    }

    handleTagChange(selectedTags) {
        if (JSON.stringify(this.state.selectedTags) != JSON.stringify(selectedTags)) {
            this.setState({
                selectedTags: selectedTags
            }, this.saveFilterState);
        }
    }

    handleOrderByChange(orderBy) {
        if (this.state.orderBy != orderBy) {
            if (orderBy == null) {
                orderBy = DEFAULT_ORDER_BY;
            }
            this.setState({
                orderBy: orderBy
            }, this.saveFilterState);
        }
    }

    loadFilterState() {
        var filterState = Cookie.load(COOKIE_FILTER_NAME);
        if (typeof filterState === 'undefined') {
            return {
                selectedTags: [],
                orderBy: DEFAULT_ORDER_BY
            }
        }
        return filterState;
    }

    saveFilterState() {
        Cookie.save(COOKIE_FILTER_NAME, this.state);
    }

    render() {
        return (
            <div>
                <Menu />

                <div id="content">
                    <div id="inner-left">
                        <ArticleList selectedTags={this.state.selectedTags} orderBy={this.state.orderBy.value}/>
                    </div>
                    <div id="inner-right">
                        <SideMenu tagHandler={this.handleTagChange.bind(this)}
                                  orderByHandler={this.handleOrderByChange.bind(this)}
                                  selectedTags={this.state.selectedTags} activeOrderBy={this.state.orderBy}/>
                    </div>
                </div>
                <GoogleAnalytics id={UA} />
            </div>
        );
    }
}

export default Transmit.createContainer(Main);