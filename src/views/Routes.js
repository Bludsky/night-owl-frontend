import React from "react";
import {Route, DefaultRoute} from "react-router";

// Views
import Main from "views/Main";
import About from "views/About";
import Detail from "views/Detail";
import Lucky from "views/Lucky";
import Admin from "views/Admin";
import Author from "views/Author";

/**
 * The React Routes for both the server and the client.
 *
 * @class Routes
 */
export default (
    <Route path="/">
        <DefaultRoute handler={Main}/>
        <Route name="Admin" path="/admin" handler={Admin}/>
        <Route name="Night Owl's Blog"  path="/" handler={Main}/>
        <Route name="Lucky" path="/lucky" handler={Lucky}/>
        <Route name="About" path="/about" handler={About}/>
        <Route name="Entry" path="/:title" handler={Detail}/>
        <Route name="Author" path="/author/:author" handler={Author}/>
    </Route>
);
