import React from "react";
import InlineCss from "react-inline-css";
import Transmit from "react-transmit";
import {UA} from "config/Config.js";
import GoogleAnalytics from "react-g-analytics";

// Components
import Menu from "components/Menu";

class About extends React.Component {

    componentDidMount() {
        document.title = "About Night Owl";
    }

    render() {
        return (
            <div>
                <Menu />

                <div id="content">
                    <div id="inner">
                        <InlineCss stylesheet={About.css()} namespace="About">
                            <div id="photo"/>
                            <p>Welcome!</p>
                            <p>My name is <strong>Petr Bludsky</strong> and I am a <strong>software developer</strong> from the Czech republic.</p>
                            <p>This website's aim is to provide you with a plenty of resources, guides and tips&amp;tricks from the software development field.</p>
                            <p>If you are interested more in me for whatever reason please check out my profiles
                                below.</p>
                            <p>Oh and I am also strongly against the 9 to 5 culture. I believe it suppresses creativity and productivity and is an obsolete relic of the past. At least for my profession, that is.</p>
                            <ul>
                                <li>
                                    <a href="https://cz.linkedin.com/in/petrbludsky" title="LinkedIn">
                                        <i className="fa fa-linkedin-square"></i>LinkedIn
                                    </a>
                                </li>
                                <li>
                                    <a href="https://gitlab.com/u/Bludsky" title="GitLab">
                                        <i className="fa fa-git"></i>GitLab
                                    </a>
                                </li>
                            </ul>
                        </InlineCss>
                    </div>
                </div>
                <GoogleAnalytics id={UA} />
            </div>
        );
    }

    static css() {
        return (`
            & ul li a {
                border: none;
            }
            & p {
                margin: 0 0 3px 0;
            }
            & ul {
                margin: 0;
                padding: 0;
            }
            & ul li {
                list-style: none !important;
            }
            & ul li i {
                margin-right: 5px;
            }
            & div#photo {
                display: block;
                width: 100px;
                height: 100px;
                float: left;
                margin: 15px 12px 12px 0;
                background: url("/img/pb.jpg") no-repeat;
                border-radius: 10px;
            }
        `);
    }

}

About.contextTypes = {
    router: React.PropTypes.func.isRequired
};

export default Transmit.createContainer(About);