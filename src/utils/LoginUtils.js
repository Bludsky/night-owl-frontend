import Request from "superagent";
import Cookie from 'react-cookie';
import {REST_URL} from "config/Config";

const COOKIE_TOKEN_NAME = "token";

class LoginUtils {

    saveToken(token) {
        Cookie.save(COOKIE_TOKEN_NAME, token);
    }

    clearToken() {
        Cookie.remove(COOKIE_TOKEN_NAME);
    }

    getToken() {
        return Cookie.load(COOKIE_TOKEN_NAME);
    }

    isAuthorized() {
        return this.getToken() != null;
    }

}

export default new LoginUtils();