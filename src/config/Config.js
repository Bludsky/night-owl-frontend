export const REST_PROTOCOL = "http";
export const REST_HOST = "localhost";
export const REST_PORT = 8081;
export const REST_CONTEXT = "NightOwl"
export const REST_URL = `${REST_PROTOCOL}://${REST_HOST}:${REST_PORT}/${REST_CONTEXT}`;

export const UA = "UA-71568894-1";